// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UdFirstProjectGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class UDFIRSTPROJECT_API AUdFirstProjectGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
