// Copyright Epic Games, Inc. All Rights Reserved.

#include "UdFirstProject.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, UdFirstProject, "UdFirstProject" );
